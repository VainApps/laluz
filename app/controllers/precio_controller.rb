class PrecioController < ApplicationController
  def obtain
    baseurl = 'https://api.preciodelaluz.org/v1/prices/all?zone=PCB'
    request = HTTParty.get baseurl
    response = JSON.parse request.body
    a = Hash.new(0)
    a['prices'] = Hash.new(0)
    response.each do |key,val|
      a['prices'][val['hour']] = val['price']
    end
    a
  end

  def index
    a = obtain
    render json: a
  end

  def importar
    obj = obtain
    headers = ['fecha', 'hora', 'precio']
    csv_data = CSV.generate(headers: true) do |csv|
      csv << headers
      obj['prices'].each do |x,y|
        csv << [DateTime.now.strftime('%Y-%m-%d'), x, y]  
      end
    end
    send_data csv_data
  end
end
