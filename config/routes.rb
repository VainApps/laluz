Rails.application.routes.draw do
  root "precio#index"
  get "precio/importar"

  resource :precio, only: [:index, :importar]
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"
end
